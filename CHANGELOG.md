2019-02-21  sergey  <siarhei.ihnatsenka@gmail.com>

  * Refactoring of minimal example
  * Minimal example added to readme 

2018-12-15  sergey  <siarhei.ihnatsenka@gmail.com>

  * Slpit track into two, before and after the thumb, for better value discrimination
  * Updated video

2018-11-26  sergey  <siarhei.ihnatsenka@gmail.com>

  * Added drag callbacks
 
