// osgWidgetSlider - Code by: Sergey Ignatenko 2018
// For callbacks, see http://tedfelix.com/software/c++-callbacks.html

#include <osg/io_utils>
#include <osgDB/ReadFile>

#include "../Slider.h"

const unsigned int MASK_2D = 0xF0000000;

// Callback example 2. Function Pointer.
class Callee {
public:
  // This static function is the real callback function. It's compatible
  // with the C-style CallbackFunctionPtr. The extra void* is used to
  // get back into the real object of this class type.
  static int staticCallbackFunction(void *p, int i) {
    // Get back into the class by treating p as the "this" pointer.
    return ((Callee *)p)->callbackFunction(i);
  }

  // The callback function that Caller will call via staticCallbackFunction() above.
  int callbackFunction(int i) {
    printf("Inside callback %d\n", i);
    return i; 
  }
} callee;

// Callback example 3. Interface Class.
class CalleeI : public CallbackInterface {
public:
  // The callback function that Caller will call.
  int cbiCallbackFunction(int i) { 
    printf("Inside callback %d\n", i);
    return i; 
  }
} calleeI;

int main(int argc, char** argv) {
  osgViewer::Viewer viewer;

  osgWidget::WindowManager* wm = new osgWidget::WindowManager(
      &viewer,
      800.0f,
      600.0f,
      MASK_2D,
      osgWidget::WindowManager::WM_PICK_DEBUG //0x00000000
  );
    
  Slider slider(wm, 100, std::pair<float, float>(0.0, 100.0), Slider::HORIZONTAL);
//  slider.setWidth(30);
//  slider.setRange(std::pair<float, float>(50,101));
//  slider.setOrigin(osgWidget::XYCoord(50, 50));

  // Callback example 1. C++11 Lambda Functions.
  //slider.attachDragCallback([&slider]{ std::cout << "callback " << slider.getValue() << std::endl; });
  // Callback example 2. Function Pointer.
  //slider.connectDragCallback(Callee::staticCallbackFunction, &callee);
  // Callback example 3. Interface Class.
  //slider.connectDragCallback(&calleeI);

  return osgWidget::createExample(viewer, wm, nullptr);
}
