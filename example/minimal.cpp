// osgWidgetSlider - Code by: Sergey Ignatenko 2018

#include <osgWidget/Util>
#include <osgWidget/WindowManager>
#include <osgWidget/Box>
#include <osgWidget/Label>

class Thumb: public osgWidget::Widget {
  float _value, _valueMax;
public:
  Thumb(const std::string& s, osgWidget::point_type x, osgWidget::point_type y)
    : osgWidget::Widget(s, x, y) {
    setEventMask(osgWidget::EVENT_ALL);
    _value = _valueMax = 100.0f;
  }
  
  bool mouseDrag(double x, double y, const osgWidget::WindowManager* wm) {
    float x0 = getParent()->getOrigin().x();
    float y0 = getParent()->getOrigin().y();
    
    if (100.0f <= y0 + y && y0 + y <= 100.0f + 200.0f - 25.0f) {
      getParent()->setOrigin(x0, y0 + y);
      getParent()->update();

      _value = _valueMax * (y0 + y - 100.0f) / (200.0f - 25.0f);
    }
  
    osgWidget::WindowManager* wm2 = const_cast<osgWidget::WindowManager*>(wm);
    osgWidget::Box* box = dynamic_cast<osgWidget::Box*>(wm2->getByName("box"));
    osgWidget::Label* label = dynamic_cast<osgWidget::Label*>(box->getByName("number"));
    label->setLabel(std::to_string(static_cast<int>(round(_value))));
    label->positioned();

    return true;
  }
  
  float getValue() const { return _value; }
};



int main(int argc, char** argv) {
    osgViewer::Viewer viewer;

    osgWidget::WindowManager* wm = new osgWidget::WindowManager(
        &viewer,
        800.0f,
        600.0f,
        0xF0000000,
        osgWidget::WindowManager::WM_PICK_DEBUG
    );

    Thumb* thumb = new Thumb("thumb", 50.0f, 25.0f);

    osgWidget::Box* boxThumb = new osgWidget::Box("box-thumb");
    boxThumb->addWidget(thumb);
    boxThumb->setOrigin(100.0f, 100.0f + 200.0f - 25.0f);
    boxThumb->attachMoveCallback();
    
    osgWidget::Widget* track = new osgWidget::Widget("track", 4.0f, 200.0f);
    track->setColor(0.5f, 0.5f, 0.5f, 1.0f);

    osgWidget::Label* label = new osgWidget::Label("number", "");
    label->setFont("fonts/Vera.ttf");
    label->setFontSize(25);
    label->setLabel(std::to_string(static_cast<int>(round(thumb->getValue()))));
    label->setPadding(20.0f);

    int labelOriginX = 100.0f + (50.0f - label->getWidthTotal()) / 2;

    osgWidget::Box* box = new osgWidget::Box("box", osgWidget::Box::VERTICAL);
    box->addWidget(track);
    box->addWidget(label);
    box->setEventMask(osgWidget::EVENT_UNFOCUS);
    box->getBackground()->setColor(0.0f, 0.0f, 0.0f, 0.0f);
    box->setOrigin(labelOriginX, 100.0f);

    wm->addChild(box);
    wm->addChild(boxThumb);

    return osgWidget::createExample(viewer, wm, nullptr);
}
