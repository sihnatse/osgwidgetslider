#ifndef OSGWIDGETSLIDER
#define OSGWIDGETSLIDER

#include <functional>

#include <osgWidget/WindowManager>
#include <osgWidget/Box>

// Callback example 2. Function Pointer.
typedef int(*CallbackFunctionPtr)(void*, int);

// Callback example 3. Interface Class.
class CallbackInterface {
public:
  virtual int cbiCallbackFunction(int) = 0;
};

class Slider {
public:
  enum Orientation {
    VERTICAL,
    HORIZONTAL
  };

  Slider(osgWidget::WindowManager* wm,
         float value = 100.0f,
         std::pair<float, float> range = std::make_pair(0.0f, 100.0f),
         Orientation = VERTICAL);

  float getValue() const { return _value; }
  void  setValue(float v);
  
  std::pair<float, float> getRange() const { return _range; }
  void                    setRange(std::pair<float, float> range);
  
  float getTrackLength() const { return _trackLength1; }
  void  setTrackLength(float length);

  float getWidth() const { return _width; }
  void  setWidth(float width);
  
  osgWidget::XYCoord getOrigin() const { return _origin; }
  void               setOrigin(const osgWidget::XYCoord& xy);

  // Callback example 1. C++11 Lambda Functions.
  void attachDragCallback(std::function<void()>&& cb) { _callback = std::move(cb); }

  // Callback example 2. Function Pointer.
  void connectDragCallback(CallbackFunctionPtr cb, void *p) {
    _cb = cb;
    _p = p;
  }

  // Callback example 3. Interface Class.
  void connectDragCallback(CallbackInterface *cb) { _cbI = cb; }
  
protected:
  class Thumb: public osgWidget::Widget {
    Slider* _parent;
  public:
    Thumb(Slider* p, const std::string& s, osgWidget::point_type x, osgWidget::point_type y);
    bool mouseDrag(double x, double y, const osgWidget::WindowManager* wm);
  };

  void _updateLabel();
  void _updateThumb();
  void _updateTracks();

private:
  
  // The current value to which the slider sets to. The range is [0, _valueMax].
  float _value;
  
  std::pair<float, float> _range;
  
  osg::ref_ptr<osgWidget::Box> _boxThumb;
  osg::ref_ptr<osgWidget::Box> _box;

  Orientation _orientation;
  
  // Track length defines a ribbon where the dragger moves. Split into before and after
  // the thumb.
  float _trackLength1, _trackLength2;
  
  // _width defines the size of widgets, including track thickness and font size for 
  // the numerical label. This way the slider resizes. Even it's float, the sizes set 
  // in integers for accurate pixel alignment.
  float _width;
  
  float _thumbHeight;
  unsigned int _padding;
  
  osgWidget::XYCoord _origin;
  
  // Callback example 1. C++11 Lambda Functions.
  std::function<void()> _callback;
  
  // Callback example 2. Function Pointer.
  CallbackFunctionPtr _cb; // The callback provided by the client via connectCallback().
  void* _p; // The additional pointer they provided (it's "this").
  
  // Callback example 3. Interface Class.
  CallbackInterface *_cbI; // The callback provided by the client via connectCallback().
};

#endif
