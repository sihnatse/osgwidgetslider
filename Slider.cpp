#include <osg/io_utils>
#include <osgDB/ReadFile>
#include <osgWidget/Util>
#include <osgWidget/Label>

#include "Slider.h"

#define TRACK_LENGTH 200.0f
#define TRACK_WIDTH 4.0f
#define THUMB_HEIGHT 25.0f
#define THUMB_WIDTH 50.0f
#define NUMBER_OFFSET 20.0f
#define ORIGIN_X 100.0f
#define ORIGIN_Y 200.0f
#define FONT_SIZE 25.0f

Slider::Slider(osgWidget::WindowManager* wm, float value, std::pair<float, float> range, Orientation o):
  _value(value),
  _range(range),
  _orientation(o) {
    float frac = (_value - _range.first) / (_range.second - _range.first);
    _trackLength1 = frac * TRACK_LENGTH;
    _trackLength2 = (1.0f - frac) * TRACK_LENGTH;
    _width = THUMB_WIDTH;
    _thumbHeight = THUMB_HEIGHT;
    _origin = osgWidget::XYCoord(ORIGIN_X, ORIGIN_Y);
    
    
  Thumb* slider;
  _boxThumb = new osgWidget::Box("box-slider");
  if (_orientation == VERTICAL) {
    slider = new Thumb(this, "slider", _width, THUMB_HEIGHT);
    _boxThumb->setOrigin(ORIGIN_X, ORIGIN_Y + _trackLength1 + _trackLength2 - THUMB_HEIGHT);
  } else {
    slider = new Thumb(this, "slider", THUMB_HEIGHT, _width);
    _boxThumb->setOrigin(ORIGIN_X + _trackLength1 + _trackLength2 - THUMB_HEIGHT, ORIGIN_Y);
  }
  slider->setColor(0.9f, 0.9f, 0.9f, 1.0f);
  _boxThumb->addWidget(slider);
  _boxThumb->attachMoveCallback();
    
    
  osgWidget::Widget *track1, *track2;
  if (_orientation == VERTICAL) {
    track1 = new osgWidget::Widget("track1", TRACK_WIDTH, _trackLength1);
    track2 = new osgWidget::Widget("track2", TRACK_WIDTH, _trackLength2);
  } else {
    track1 = new osgWidget::Widget("track1", _trackLength1, TRACK_WIDTH);
    track2 = new osgWidget::Widget("track2", _trackLength2, TRACK_WIDTH);
  }
  track1->setColor(0.9f, 0.9f, 0.9f, 1.0f);
  track2->setColor(0.5f, 0.5f, 0.5f, 1.0f); // tracks differ by color
  track1->setLayer(osgWidget::Widget::LAYER_TOP);
  track2->setLayer(osgWidget::Widget::LAYER_TOP);

  osgWidget::Label* label = new osgWidget::Label("number", "");
  label->setFont("fonts/Vera.ttf");
  label->setFontSize(FONT_SIZE);
  label->setColor(1.0f, 0.5f, 0.0f, 0.0f);
  label->setLabel(std::to_string(static_cast<int>(round(value))));

  _padding = label->getTextSize().y() / 2;
  if (_orientation == VERTICAL) {
    int x = std::max(label->getTextSize().x(), _width);
    label->setSize(x, label->getTextSize().y());
    track1->setPadBottom(_padding);
  } else {
    int y = std::max(label->getTextSize().y(), _width);
    label->setSize(label->getTextSize().x(), y);
    track1->setPadLeft(_padding);
  }
  label->setPadding(_padding);

  _box = new osgWidget::Box("box", _orientation == VERTICAL ? osgWidget::Box::VERTICAL : osgWidget::Box::HORIZONTAL);
  _box->addWidget(track1);
  _box->addWidget(track2);
  _box->addWidget(label);
  _box->setOrigin(ORIGIN_X - _padding, ORIGIN_Y - _padding);
  _box->getBackground()->setColor(0.0f, 0.0f, 0.0f, 0.1f);
  _box->setEventMask(osgWidget::EVENT_UNFOCUS);

  wm->addChild(_box);
  wm->addChild(_boxThumb);
}

void Slider::setValue(float v) {
  _value = v;
  _updateLabel();
  _updateThumb();
}


void Slider::setRange(std::pair<float, float> range) {
  _range = range;
  if (range.second < _value) {
    _value = range.second;
    _updateLabel();
  } else if (range.first > _value) {
    _value = range.first;
    _updateLabel();
  }
  _updateThumb();
}


void Slider::setTrackLength(float length) {
  _trackLength1 = length;
  
  osgWidget::Widget* track = dynamic_cast<osgWidget::Widget*>(_box->getByIndex(1));
  track->setHeight(length);
  
  _updateThumb();
}

void Slider::setWidth(float width) {
  _width = width;
  
  osgWidget::Widget* track = dynamic_cast<osgWidget::Widget*>(_box->getByIndex(1));
  int tw = static_cast<int>(round(width * TRACK_WIDTH / THUMB_WIDTH));
  track->setWidth(tw);
  
  osgWidget::Label* label = dynamic_cast<osgWidget::Label*>(_box->getByIndex(3));
  int fs = static_cast<int>(round(width * FONT_SIZE / THUMB_WIDTH));
  label->setFontSize(fs);
  label->positioned();

  osgWidget::Widget* slider = dynamic_cast<osgWidget::Widget*>(_boxThumb->getByIndex(1));
  slider->setWidth(width);
  _thumbHeight = static_cast<int>(round(width * THUMB_HEIGHT / THUMB_WIDTH));
  slider->setHeight(_thumbHeight);

  _box->resize(); // recalculate box size based new sizes of contained elements

  int x = static_cast<int>(round(_box->getX() + (_box->getWidth() - width) / 2.0f));
  _boxThumb->setX(x);

  _updateThumb();
}

void Slider::setOrigin(const osgWidget::XYCoord& xy) {
  // The boxes are shifted by padding, so the origin is thumb bottom left 
  osgWidget::XYCoord old = _box->getOrigin();
  if (_orientation == VERTICAL) {
    _boxThumb->addOrigin(xy.x() - old.x() - _padding, xy.y() - old.y());
  } else {
    _boxThumb->addOrigin(xy.x() - old.x(), xy.y() - old.y() - _padding);
  }
  _box->setOrigin(xy.x() - _padding, xy.y() - _padding);
  _origin = xy;
  _updateThumb();
}


void Slider::_updateThumb() {
  float fraction = (_value - _range.first) / (_range.second - _range.first);
  
  if (_orientation == VERTICAL) {
    float y = (_trackLength1 + _trackLength2 - _thumbHeight) * fraction + _origin.y();
    _boxThumb->setOrigin(_boxThumb->getOrigin().x(), y);
  } else {
    float x = (_trackLength1 + _trackLength2 - _thumbHeight) * fraction + _origin.x();
    _boxThumb->setOrigin(x, _boxThumb->getOrigin().y());
  }
  _boxThumb->update();
}

void Slider::_updateLabel() {
  osgWidget::Label* label = dynamic_cast<osgWidget::Label*>(_box->getByIndex(3));
  label->setLabel(std::to_string(static_cast<int>(round(_value))));
  label->positioned();
}

void Slider::_updateTracks() {
  osgWidget::Widget* track1 = dynamic_cast<osgWidget::Widget*>(_box->getByIndex(1));
  osgWidget::Widget* track2 = dynamic_cast<osgWidget::Widget*>(_box->getByIndex(2));
  float length = _trackLength1 + _trackLength2;
  float frac = (_value - _range.first) / (_range.second - _range.first);
  if (_orientation == VERTICAL) {
    track1->setHeight(static_cast<int>(frac * length));
    track2->setHeight(static_cast<int>((1.0f - frac) * length));
  } else {
    track1->setWidth(static_cast<int>(frac * length));
    track2->setWidth(static_cast<int>((1.0f - frac) * length));
  }
  _box->resize();
}

Slider::Thumb::Thumb(Slider* p, const std::string& s, osgWidget::point_type x, osgWidget::point_type y)
  : osgWidget::Widget(s, x, y), _parent(p) {
  setEventMask(osgWidget::EVENT_ALL); //EVENT_MOUSE_DRAG
}

bool Slider::Thumb::mouseDrag(double x, double y, const osgWidget::WindowManager* wm) {
  float x0 = getParent()->getOrigin().x();
  float y0 = getParent()->getOrigin().y();

  if (_parent->_orientation == VERTICAL) {
    if (_parent->_origin.y() <= y0 + y && y0 + y <= _parent->_origin.y() + _parent->_trackLength1 + _parent->_trackLength2 - _parent->_thumbHeight) {
      osgWidget::XYCoord xy(x0, y0 + y);
      getParent()->setOrigin(xy);
      getParent()->update();

      _parent->_value = (_parent->_range.second - _parent->_range.first) *
      (y0 + y - _parent->_origin.y()) / (_parent->_trackLength1 + _parent->_trackLength2 - _parent->_thumbHeight) + _parent->_range.first;
    }
  } else {
    if (_parent->_origin.x() <= x0 + x && x0 + x <= _parent->_origin.x() + _parent->_trackLength1 + _parent->_trackLength2 - _parent->_thumbHeight) {
      osgWidget::XYCoord xy(x0 + x, y0);
      getParent()->setOrigin(xy);
      getParent()->update();

      _parent->_value = (_parent->_range.second - _parent->_range.first) *
      (x0 + x - _parent->_origin.x()) / (_parent->_trackLength1 + _parent->_trackLength2 - _parent->_thumbHeight) + _parent->_range.first;
    }
  }
  
  _parent->_updateLabel();
  _parent->_updateTracks();
  
  // Handle callbacks.
  if (_parent->_callback)
    _parent->_callback();
  if (_parent->_cb)
    int i = _parent->_cb(_parent->_p, _parent->_value);
  if (_parent->_cbI)
    int i = _parent->_cbI->cbiCallbackFunction(_parent->_value);

  return true;
}